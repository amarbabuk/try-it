import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.security.KeyFactory;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Base64;

public class ImportKeyAndCert {

    public static void main(String[] args) throws Exception {
        String keyFile = "path/to/private.key"; // Path to your private key
        String certFile = "path/to/certificate.crt"; // Path to your signed certificate
        String keystoreFile = "path/to/keystore.jks"; // Path to output JKS file
        String alias = "myalias"; // Alias for the key and certificate entry
        String storepass = "changeit"; // Password for the keystore
        String keypass = "changeit"; // Password for the key entry

        // Read private key
        String key = new String(java.nio.file.Files.readAllBytes(java.nio.file.Paths.get(keyFile)))
            .replace("-----BEGIN PRIVATE KEY-----", "")
            .replace("-----END PRIVATE KEY-----", "")
            .replaceAll("\\s", "");
        byte[] keyBytes = Base64.getDecoder().decode(key);
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(keyBytes);
        PrivateKey privateKey = KeyFactory.getInstance("RSA").generatePrivate(keySpec);

        // Read certificate
        FileInputStream certInputStream = new FileInputStream(certFile);
        CertificateFactory certFactory = CertificateFactory.getInstance("X.509");
        X509Certificate certificate = (X509Certificate) certFactory.generateCertificate(certInputStream);
        certInputStream.close();

        // Create keystore and set the key and certificate entry
        KeyStore keystore = KeyStore.getInstance("JKS");
        keystore.load(null, storepass.toCharArray());
        keystore.setKeyEntry(alias, privateKey, keypass.toCharArray(), new X509Certificate[]{certificate});

        // Save the keystore to a file
        FileOutputStream keystoreOutputStream = new FileOutputStream(keystoreFile);
        keystore.store(keystoreOutputStream, storepass.toCharArray());
        keystoreOutputStream.close();
        
        System.out.println("Keystore created successfully.");
    }
}